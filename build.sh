#!/bin/bash

gcc -Wall -fsanitize=address -g $(dirname $0)/src/*.c -o $(dirname $0)/bin/turing_machine
