#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "str.h"

#define MAX_STATES 1024

#define TAPE_DELIMITER_CHAR '$'
#define TAPE_END_VAL 1

typedef enum {
	COMMAND_RUN,
	COMMAND_DRAW,
	COMMAND_NUM_COMMANDS
} Command;

Str command_strs[] = {
	[COMMAND_RUN] = STR_FROM_CSTRING_ARR("run"),
	[COMMAND_DRAW] = STR_FROM_CSTRING_ARR("draw"),
};

Command command_from_string(Str string) {
	for (int i=0; i<COMMAND_NUM_COMMANDS; i++) {
		if (!str_compare(string, command_strs[i], 1)) {
			return i;
		}
	}
	return -1;
}

typedef enum {
	DIRECTION_LEFT,
	DIRECTION_RIGHT,
} Direction;

typedef struct {
	char* arr;
	size_t current_index;
	size_t start_pos;
	size_t end_pos;
	size_t capacity;
} Tape;

typedef struct {
	char condition;
	char new_val;
	Direction direction;
	char new_state_name[64];
} Connection;

typedef struct {
	char name[64];
	bool accepts;
	Connection* connection;
	size_t num_connections;
	size_t capacity;
} State;

int state_find(State* state_list, size_t num_states, Str state_name) {
	assert(num_states <= MAX_STATES);
	for (int i=0; i<num_states; i++) {
		Str name = str_from_cstring(state_list[i].name);
		if (!str_compare(name, state_name, 0)) {
			return i;
		}
	}
	return -1;
}

typedef enum {
	ERROR_NONE,
	ERROR_UNKNOWN_SYMBOL,
	ERROR_TRYING_TO_ADD_CONNECTION_TO_ACCEPT,
	ERROR_TRYING_TO_ADD_ACCEPT_TO_CONNECTION,
	ERROR_TOO_MANY_STATES,
	ERROR_CONDITION_ALREADY_EXIST,
} Error;

char* error_strings[] = {
	"ERROR_NONE",
	"ERROR_UNKNOWN_SYMBOL",
	"ERROR_TRYING_TO_ADD_CONNECTION_TO_ACCEPT",
	"ERROR_TRYING_TO_ADD_ACCEPT_TO_CONNECTION",
	"ERROR_TOO_MANY_STATES",
	"ERROR_CONDITION_ALREADY_EXIST",
};

Error connection_parse(Connection* conn, Str condition, Str destination) {
	Str direction = str_trim(str_split_by_char(condition, ',', 1));
	Str condi = str_trim(str_split_by_char(condition, ',', 0));
	if (direction.len != 1) {
		wrap_print(direction);
		return ERROR_UNKNOWN_SYMBOL;
	}
	char c = direction.str[0];
	if (c == 'l' || c == 'L') {
		conn->direction = DIRECTION_LEFT;
	} else if (c == 'r' || c == 'R') {
		conn->direction = DIRECTION_RIGHT;
	} else {
		return ERROR_UNKNOWN_SYMBOL;
	}
	Str initial = str_trim(str_split_by_char(condi, '-', 0));
	Str end = str_trim(str_split_by_char(condi, '-', 1));
	end.str++;
	end.len--;
	if (initial.len != 1 || end.len != 1) {
		wrap_print(initial);
		wrap_print(end);
		return ERROR_UNKNOWN_SYMBOL;
	}
	char init = initial.str[0];
	char ending = end.str[0];
	conn->condition = init;
	conn->new_val = ending;
	str_to_cstr(destination, conn->new_state_name, 64);
	return ERROR_NONE;
}

Error state_parse(Str instruction, State* state_list, size_t* num_states, bool* has_accept) {
	Str state_name = str_trim(str_split_by_char(instruction, ':', 0));
	Str condition = str_trim(str_split_by_char(instruction, ':', 1));
	int index = state_find(state_list, *num_states, state_name);
	State* state_found = state_list+index;
	Str destination_state_name = str_trim(str_split_by_char(instruction, ':', 2));
	if (!str_compare(condition, STR_FROM_CSTRING_ARR("ACCEPT"), true)) {
		if (index != -1) {
			return ERROR_TRYING_TO_ADD_ACCEPT_TO_CONNECTION;
		}
		if (*has_accept) {
			return ERROR_TRYING_TO_ADD_ACCEPT_TO_CONNECTION;
		}
		State new_state;
		str_to_cstr(state_name, new_state.name, sizeof(new_state.name));
		new_state.accepts = true;
		new_state.connection = 0;
		new_state.num_connections = 0;
		if ((*num_states)+1 >= MAX_STATES) {
			return ERROR_TOO_MANY_STATES;
		}
		state_list[(*num_states)++] = new_state;
		*has_accept = true;
		return ERROR_NONE;
	}
	if (index != -1) {
		if (state_found->num_connections == state_found->capacity) {
			Connection* temp = reallocarray(state_found->connection, state_found->capacity*2, sizeof(*temp));
			if (!temp) {
				fprintf(stderr, "Error in allocation\n");
				exit(1);
			}
			state_found->connection = temp;
			state_found->capacity*=2;
		}
		Connection conn;
		Error err = connection_parse(&conn, condition, destination_state_name);
		if (err != ERROR_NONE) {
			return err;
		}
		for (int i=0; i<state_found->num_connections; i++) {
			if (state_found->connection[i].condition == conn.condition) {
				printf("%c %c\n", conn.condition, state_found->connection[i].condition);
				return ERROR_CONDITION_ALREADY_EXIST;
			}
		}
		state_found->connection[state_found->num_connections++] = conn;
	} else {
		State new_state;
		str_to_cstr(state_name, new_state.name, sizeof(new_state.name));
		new_state.accepts = false;
		new_state.connection = calloc(1024, sizeof(Connection));
		new_state.num_connections = 0;
		if ((*num_states)+1 >= MAX_STATES) {
			return ERROR_TOO_MANY_STATES;
		}
		Connection conn;
		Error err = connection_parse(&conn, condition, destination_state_name);
		if (err != ERROR_NONE) {
			return err;
		}
		new_state.connection[new_state.num_connections++] = conn;
		state_list[(*num_states)++] = new_state;
	}
	return ERROR_NONE;
}

void connection_print(Connection conn) {
	printf("\t%c->%c, %c: %s\n", conn.condition, conn.new_val, (conn.direction == DIRECTION_RIGHT) ? 'R' : 'L', conn.new_state_name);
}

void state_print(State s) {
	printf("%s: \n", s.name);
	if (s.accepts) {
		printf("\tACCEPT\n");
	} else {
		for (int i=0; i<s.num_connections; i++) {
			connection_print(s.connection[i]);
		}
	}
}

void state_free(State* s) {
	free(s->connection);
	s->connection = 0;
	s->num_connections = 0;
	s->capacity = 0;
}

void tape_print(Tape tape) {
	printf("START=%ld\n", tape.start_pos);
	printf("END=%ld\n", tape.end_pos);
	printf("CURRENT=%ld\n", tape.current_index);
	printf("[ ");
	for (int i=tape.start_pos; i<=tape.end_pos; i++) {
		printf("%c ", tape.arr[i]);
	}
	printf("\b]\n");
	printf("  ");
	for (int i=tape.start_pos; i<=tape.end_pos; i++) {
		if (i != tape.current_index) {
			printf("  ");
			continue;
		}
		printf("^");
	}
	printf("\n");
}

void turing_run(State* states, size_t num_states, Tape tape) {
	State current = states[0];
	tape_print(tape);
	while (true) {
		printf("-------------------\n");
		if (current.accepts) {
			printf("%s: ACCEPT\n", current.name);
			tape_print(tape);
			break;
		}
		char cond = tape.arr[tape.current_index];
		Connection next;
		bool hasNext = false;
		int i;
		for (i=0; i<current.num_connections; i++) {
			if (cond == current.connection[i].condition) {
				hasNext = true;
				next = current.connection[i];
			}
		}
		if (!hasNext) {
			break;
		}
		printf("%s: ", current.name);
		connection_print(next);
		tape.arr[tape.current_index] = next.new_val;
		if (next.direction == DIRECTION_LEFT) {
			if (tape.current_index >= tape.start_pos) {
				tape.current_index--;
			}
		}
		if (next.direction == DIRECTION_RIGHT) {
			if (tape.current_index <= tape.end_pos) {
				tape.current_index++;
			}
		}
		tape_print(tape);
		int index = state_find(states, num_states, str_from_cstring(next.new_state_name));
		assert(index != -1);
		current = states[index];
	}
}

void tape_new(Str str, Tape* newTape) {
	char* buf = calloc(str.len+2, sizeof(*buf));
	if (!buf) {
		fprintf(stderr, "Could not allocate\n");
		exit(1);
	}
	buf[0] = TAPE_DELIMITER_CHAR;
	buf[str.len+1] = TAPE_DELIMITER_CHAR;
	for (int i=1; i<str.len+1; i++) {
		buf[i] = str.str[i-1];
	}
	newTape->arr = buf;
	newTape->end_pos = str.len+1;
	newTape->start_pos = 0;
	newTape->capacity = str.len+2;
	newTape->current_index = 1;
}

void tape_destroy(Tape* tape) {
	free(tape->arr);
	tape->arr = 0;
	tape->capacity = 0;
	tape->start_pos = 0;
	tape->end_pos = 0;
	tape->current_index = 0;
}

Error states_load(State* states, size_t* num_states, const char* filename) {
	FILE* file = fopen(filename, "r");
	if (!file) {
		fprintf(stderr, "Could not open file %s\n", filename);
		exit(1);
	}
	char buffer[8192] = {0};
	bool has_accept = false;
	for (int i=0; i<MAX_STATES && fgets(buffer, 8192, file); i++) {
		Str line = str_split_by_char(str_trim(str_from_cstring(buffer)), '#', 0);
		if (line.len == 0) {
			i--;
			continue;
		}
		Error err = state_parse(line, states, num_states, &has_accept);
		if (err) {
			fprintf(stderr, "%s\n", error_strings[err]);
			return err;
		}
	}
	fclose(file);
	return ERROR_NONE;
}

void states_to_graphviz(State* states, size_t num_states, FILE* file) {
	// for (int i=0; i<num_states; i++) {
	// 	state_print(states[i]);
	// }
	fprintf(file, "digraph {\n");
	for (int i=0; i<num_states; i++) {
		if (states[i].accepts) {
			fprintf(file, "\t%s [shape=\"doublecircle\"]\n", states[i].name);
		}
		for (int j=0; j<states[i].num_connections; j++) {
			fprintf(file, "\t%s->%s [label=\"%c->%c, %c\"]\n", states[i].name, states[i].connection[j].new_state_name, states[i].connection[j].condition, states[i].connection[j].new_val, (states[i].connection[j].direction == DIRECTION_LEFT) ? 'L' : 'R');
		}
	}
	fprintf(file, "}");
}

int main(int argc, char* argv[argc]) {
	if (argc < 3) {
		fprintf(stderr, "USAGE: %s <command> <file> <input/output filename, depending on command>\nCommands: {run, draw}", argv[0]);
		return 1;
	}
	Command command = command_from_string(str_from_cstring(argv[1]));
	if (command == -1) {
		fprintf(stderr, "Command not found: %s\n", argv[1]);
		return 1;
	}
	State states[MAX_STATES];
	size_t num_states = 0;
	Error err = states_load(states, &num_states, argv[2]);
	if (err) {
		return err;
	}
	if (command == COMMAND_RUN) {
		Tape t;
		tape_new(str_from_cstring(argv[3]), &t);
		turing_run(states, num_states, t);
		tape_destroy(&t);
	} else if (command == COMMAND_DRAW) {
		FILE* file;
		if (argc == 4) {
			file = fopen(argv[3], "w");
			if (!file) {
				fprintf(stderr, "Could not open file %s\n", argv[3]);
				exit(1);
			}
		} else {
			file = stdout;
		}
		states_to_graphviz(states, num_states, file);
		if (argc == 4) {
			fclose(file);
		}
	}
	for (int i=0; i<num_states; i++) {
		state_free(states+i);
	}
	return 0;
}
